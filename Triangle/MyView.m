//
//  MyView.m
//  Triangle
//
//  Created by Aleksander Pniok on 09.07.2014.
//  Copyright (c) 2014 Aleksander Pniok. All rights reserved.
//

#import "MyView.h"
@implementation MyView
- (void)drawRect:(CGRect)rect
{
    double x,y;
    CGPoint p1,p2,p3;
    
    p1 = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 - self.frame.size.width/2);
    p2 = CGPointMake(self.frame.size.width, self.frame.size.height/2 + self.frame.size.width/2);
    p3 = CGPointMake(0, self.frame.size.height/2 + self.frame.size.width/2);
    
    x = self.frame.size.width/2;
    y = self.frame.size.height/2;
    
    CGContextRef myContext = UIGraphicsGetCurrentContext();
    
    
    for (int i = 0; i< 100000; i ++) {
        CGPoint p = CGPointMake(0, 0);
        switch (arc4random()%3) {
            case 0:
                p = p1;
                break;
            case 1:
                p = p2;
                break;
            case 2:
                p = p3;
                break;
            default:
                p = p3;
                break;
        }
        
        x = x + (p.x - x) / 2;
        y = y + (p.y - y) / 2;
        
        CGContextFillRect (myContext, CGRectMake (x, y, 1, 1));
    }
}

@end
