//
//  main.m
//  Triangle
//
//  Created by Aleksander Pniok on 09.07.2014.
//  Copyright (c) 2014 Aleksander Pniok. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
